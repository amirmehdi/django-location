#!/bin/bash
set -e

echo "Waiting for database..."
while ! nc -z ${DB_HOST_READ} ${DB_PORT}; do sleep 2; done
echo "Connected to database."


echo "Collecting static assets..."
./manage.py collectstatic --no-input --verbosity 0


echo "Checking if migrations are created for models changes..."
./manage.py makemigrations --no-input --dry-run --check


echo "Running gunicorn..."
gunicorn djangoLocation.wsgi:application -c gunicorn_config.py &

# Start Celery Workers
celery -A core worker -l info &> /log/celery.log &

# Start Celery Beat
celery -A core beat -l info &> /log/celery_beat.log &