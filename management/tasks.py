from __future__ import absolute_import, unicode_literals

from datetime import datetime, timedelta

from celery import shared_task
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from fitz import fitz

from djangoLocation import settings


@shared_task(name='mail-new-user')
def test():
    users = list(
        User.objects.filter(date_joined__gt=datetime.now() - timedelta(days=1)).values_list('username', flat=True))
    today = datetime.now().strftime('%m-%d-%Y')
    create_pdf(f'{today}.pdf', users)
    email(today)


def create_pdf(file_name, text):
    doc = fitz.open()  # new or existing PDF
    page = doc.new_page()  # new or existing page via doc[n]
    p = fitz.Point(50, 72)  # start point of 1st line

    rc = page.insert_text(p,  # bottom-left of 1st char
                          text,  # the text (honors '\n')
                          fontname="helv",  # the default font
                          fontsize=11,  # the default font size
                          rotate=0,  # also available: 90, 180, 270
                          )
    print("%i lines printed on page %i." % (rc, page.number))

    doc.save(file_name)


def email(today):
    subject = f'new user at {today}'
    message = ' new users '
    email_from = settings.EMAIL_HOST_USER
    recipient_list = list(User.objects.filter(is_staff=True).values_list('email', flat=True))
    msg = EmailMultiAlternatives(subject, message, email_from, recipient_list)
    msg.attach_file(f'{today}.pdf')
    msg.send()
