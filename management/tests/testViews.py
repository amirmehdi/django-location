import json

from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.reverse import reverse
from rest_framework.status import HTTP_200_OK, HTTP_401_UNAUTHORIZED


class RegisterTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        User.objects.create_user(username='username1', password='password1',
                                 email='email1@gmail.com')

    def setUp(self):
        self.user1 = {
            'username': 'username1',
            'password': 'password1',
            'email': 'email1@gmail.com',
            'location': {'longitude': 37.36, 'latitude': 37.35}
        }
        self.user2 = {
            'username': 'username2',
            'password': 'password2',
            'email': 'email2@gmail.com',
            'location': {'longitude': '39.36', 'latitude': '39.35'}
        }

    def test_create_user_email_exist(self):
        self.user1['username'] = 'username11'
        response = self.client.post(reverse('user-list'), data=json.dumps(self.user1), content_type='application/json')
        self.assertContains(response, 'username or email exists', status_code=400)

    def test_create_user_username_exist(self):
        self.user1['email'] = 'gmail@gmail.com'
        response = self.client.post(reverse('user-list'), data=json.dumps(self.user1), content_type='application/json')
        self.assertContains(response, 'username or email exists', status_code=400)

    def test_create_user(self):
        response = self.client.post(reverse('user-list'), data=json.dumps(self.user2), content_type='application/json')
        self.assertEqual(response.status_code, HTTP_200_OK)
        res = json.loads(response.content)
        self.assertNotEqual(res['user'], 1)

    def test_create_user_incorrect_email(self):
        self.user1['username'] = 'username3'
        self.user1['email'] = 'gmail.com'
        response = self.client.post(reverse('user-list'), data=json.dumps(self.user1), content_type='application/json')
        self.assertContains(response, 'invalid request body', status_code=400)


class NearestTest(TestCase):

    def setUp(self):
        self.user1 = {
            'username': 'username1',
            'password': 'password1',
            'email': 'email1@gmail.com',
            'location': {'longitude': 37.36, 'latitude': 37.35}
        }
        self.user2 = {
            'username': 'username2',
            'password': 'password2',
            'email': 'email2@gmail.com',
            'location': {'longitude': '39.36', 'latitude': '39.35'}
        }
        self.client.post(reverse('user-list'), data=json.dumps(self.user1), content_type='application/json')
        self.client.post(reverse('user-list'), data=json.dumps(self.user2), content_type='application/json')

    def test_failed_login(self):
        response = self.client.post(reverse('token_obtain_pair'),
                                    data={'username': 'username1', 'password': 'password11'},
                                    content_type='application/json')
        self.assertEqual(response.status_code, HTTP_401_UNAUTHORIZED)

    def test_login_and_get_nearest(self):
        response = self.client.post(reverse('token_obtain_pair'),
                                    data={'username': 'username1', 'password': 'password1'},
                                    content_type='application/json')
        self.assertEqual(response.status_code, HTTP_200_OK)
        res = json.loads(response.content)
        access_token = res['access']
        headers = {
            'HTTP_AUTHORIZATION': f'Bearer {access_token}'
        }
        response = self.client.get(reverse('user-find-nearest'), **headers)
        self.assertEqual(response.status_code, HTTP_200_OK)
        res = json.loads(response.content)
        self.assertEquals(res['distance'], 282603)
        self.assertNotEqual(res['UserInfo']['user'], 1)
