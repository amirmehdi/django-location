from django.contrib.auth.models import User
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from django.db.models import Q
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.mixins import CreateModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from management.models import UserInfo
from management.serializers import UserInfoSerializer, UserSerializer, ResponseSerializer, NearestUser


class UserViewSet(viewsets.GenericViewSet,
                  CreateModelMixin):
    queryset = UserInfo.objects.all()

    def get_permissions(self):
        if self.action == 'create':
            return [AllowAny()]
        return [permissions.IsAuthenticated()]

    def get_serializer_class(self):
        if self.action == 'create':
            return UserSerializer
        return UserInfoSerializer

    @swagger_auto_schema(operation_description="create user", request_body=UserSerializer,
                         responses={
                             200: openapi.Response('success', UserInfoSerializer),
                             400: openapi.Response('invalid request body', ResponseSerializer)
                         })
    def create(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except ValidationError as err:
            return Response(ResponseSerializer({'code': 'failed', 'message': 'invalid request body'}).data, 400)
        if User.objects.filter(Q(username=serializer.data['username']) | Q(email=serializer.data['email'])).count() > 0:
            return Response(ResponseSerializer({'code': 'failed', 'message': 'username or email exists'}).data, 400)

        user = User.objects.create_user(username=serializer.data['username'], password=serializer.data['password'],
                                        email=serializer.data['email'])
        location = serializer.data['location']
        userInfo = UserInfo.objects.create(user=user, location=Point(location['longitude'], location['latitude']))
        return Response(UserInfoSerializer(userInfo).data, 200)

    @swagger_auto_schema(operation_description="get user info",
                         responses={200: openapi.Response('success', UserInfoSerializer)})
    @action(detail=False, methods=['GET'], url_name='/')
    def retrieve_one(self, request):
        return Response(UserInfoSerializer(UserInfo.objects.get(user_id=request.user.pk)).data, 200)

    @swagger_auto_schema(operation_description="find nearest user",
                         responses={200: openapi.Response('success', NearestUser)})
    @action(detail=False, methods=['GET'])
    def find_nearest(self, request, *args, **kwargs):
        user_info = UserInfo.objects.get(user_id=request.user.pk)
        nearest_user = UserInfo.objects\
            .filter(~Q(user_id=request.user.pk))\
            .annotate(distance=Distance('location', user_info.location))\
            .order_by('distance').first()
        return Response(NearestUser({'UserInfo': nearest_user, 'distance': int(nearest_user.distance.m)}).data, 200)
