from django.contrib.auth.models import User
from .models import *
from rest_framework import serializers
from drf_extra_fields.geo_fields import PointField


class UserSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    email = serializers.EmailField()
    location = PointField()


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInfo
        fields = '__all__'


class NearestUser(serializers.Serializer):
    UserInfo = UserInfoSerializer()
    distance = serializers.IntegerField()


class ResponseSerializer(serializers.Serializer):
    code = serializers.CharField()
    message = serializers.CharField()
