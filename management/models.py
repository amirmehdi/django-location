from django.conf import settings
from django.contrib.gis.db import models


class UserInfo(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    location = models.PointField()
