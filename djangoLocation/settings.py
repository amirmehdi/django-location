from decouple import config

DVELOPMENT_MODE = True if config('MODE') == "development" else False

if DVELOPMENT_MODE:
    from .django_settings.development_settings import *
else:
    from .django_settings.production_settings import *
