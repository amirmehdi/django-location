FROM python:3.7-alpine
ENV PYTHONUNBUFFERED 1

WORKDIR /djangoLocation
COPY requirements.txt .

RUN apk update && apk upgrade
# TODO --virtual for temporary packages
RUN apk add --no-cache \
    ca-certificates gcc g++ cmake make postgresql-dev linux-headers musl-dev \
    python3-dev libc-dev libffi-dev jpeg-dev zlib-dev mupdf-dev freetype-dev \
    gettext vim curl postgresql-client \
    binutils proj

RUN apk add --no-cache \
        --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing \
        geos gdal gdal-dev proj-dev geos-dev

ARG MUPDF=1.18.0
RUN ln -s /usr/include/freetype2/ft2build.h /usr/include/ft2build.h \
    && ln -s /usr/include/freetype2/freetype/ /usr/include/freetype \
    && wget -c -q https://www.mupdf.com/downloads/archive/mupdf-${MUPDF}-source.tar.gz \
    && tar xf mupdf-${MUPDF}-source.tar.gz \
    && cd mupdf-${MUPDF}-source \
    && make HAVE_X11=no HAVE_GLUT=no shared=yes prefix=/usr/local install \
    && cd .. \
    && rm -rf *.tar.gz mupdf-${MUPDF}-source
RUN pip install PyMuPDF==1.18.10

RUN pip install -r requirements.txt

COPY . .

EXPOSE 8000
EXPOSE 50051

RUN ["chmod", "+x", "./docker-entrypoint.sh"]
CMD ["./docker-entrypoint.sh"]
