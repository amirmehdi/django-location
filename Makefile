.PHONY: test push docker deploy-staging deploy-production deploy-canary

GIT ?= git
DOCKER = docker
COMPOSE := docker-compose

COMMIT := $(shell $(GIT) rev-parse HEAD)
CI_COMMIT_TAG ?=
VERSION ?= $(strip $(if $(CI_COMMIT_TAG),$(CI_COMMIT_TAG),$(CI_COMMIT_REF_SLUG)))
DOCKER_IMAGE := registry.gitlab.com/amirmehdi/django-location
IMAGE_TAG ?= $(DOCKER_IMAGE):$(VERSION)

docker: ## to build docker image
	echo $(CI_COMMIT_TAG)
	echo $(CI_COMMIT_REF_SLUG)
	$(DOCKER) build --file Dockerfile --tag $(IMAGE_TAG) \
		--build-arg GIT_USER=$(GIT_USER) \
		--build-arg GIT_PASS=$(GIT_PASS) \
		.

push: ## to push docker image to registry
	$(DOCKER) push $(IMAGE_TAG)

test: ## to test
	$(DOCKER) run -t --env-file ./.env-test \
		-e DB_NAME=gis \
		-e DB_USER=dineeasy \
		-e DB_PASSWORD=dineeasy123 \
		-e DB_HOST_MASTER=localhost \
		-e DB_HOST_READ=localhost \
		--rm $(IMAGE_TAG) make check

e2eTest: ## to test
	$(DOCKER) run -t \
 		--env-file ./.env-test \
 		--rm $(IMAGE_TAG) \
		make e2eCheck


check:
	cp .env-test .env
	DB_NAME=gis DB_USER=dineeasy DB_PASSWORD=dineeasy123 DB_HOST_READ=mdillon-postgis DB_HOST_MASTER=mdillon-postgis python manage.py test

e2eCheck:
	python manage.py test --pattern="e2eTest*.py"

