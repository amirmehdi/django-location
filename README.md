# django location

This application was developed using django rest framework

other techs: postgis, celery, redis, jwt, swagger

models schema is shown here  
[![diagram][product-screenshot]]()

* we use django auth user as our user model
* each user has additional properties like location saved at userInfo model  


### Installation without docker
1. install prerequisites
    ```sh
    pip install -r requirements.txt
    ```
2. set envs for development mode
    ```sh
    cp .env-test .env 
    ```
3. migrate 
    ```sh
    python manage.py migrate
    ```
4. run
    ```sh
     python manage.py runserver
    ```

### installation with docker
1. if you want to use our built docker image
    ```sh
    cd docker
    docker compose up -d -f production-compose.yml
    ```
1. if you want to develop the project with help of docker
    ```sh
    cd docker 
    docker compose up -d -f local-compose.yml
    ```
# how it works

api documentation added in docs/api-sample.http

1. user registers
2. user gets token
3. user requests for the nearest user based on their location

### front pages
admin page

    http://127.0.0.1:8000/admin

swagger page :

    http://127.0.0.1:8000/swagger
    
[product-screenshot]: docs/models.png